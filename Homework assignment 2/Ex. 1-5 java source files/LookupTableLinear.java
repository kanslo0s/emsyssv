/** This class encapsulates a lookup table that is linear, that is,
 *  the output values are evenly distributed with respect to the scale.
 *  Note that this lookup table does not store its size (that is, scale indexes
 *  of arbitrary sizes can be used to look up values in this table). 
 */
class LookupTableLinear {
	/*@ ghost int endPointOfScaleValue;*/
	/** The start (or minimal) value of the table. */
	int startValue;
	
	/** The value range of the table. */
	int range;
	
	// INVARIANT

	// MODEL + INVARIANT
	//@ model boolean endPointOfScaleRangeReached;	
	//@ represents endPointOfScaleRangeReached <- (endPointOfScaleValue >= range + startValue);
	
	/**
	 * Constructs a new linear lookup table
	 * @param startValue the starting/minimum lookup value
	 * @param range the value range
	 */
	// CONTRACT
	//@ requires range > 0;
	//@ requires startValue > 0;
	LookupTableLinear(int startValue, int range) {
		this.startValue = startValue;
		this.range = range;
	}
	
	// CONTRACT with MODEL
	/*@ requires si.getIntPart() < si.getSize();
	    ensures !endPointOfScaleRangeReached;
	    ensures endPointOfScaleValue >= startValue;
		also
		requires si.getIntPart() == si.getSize();
		requires si.getFracPart() == 0; 
		ensures endPointOfScaleRangeReached;
		ensures endPointOfScaleValue >= startValue;
	*/
	/*@ pure @*/ int getValue(ScaleIndex si) {
		//@ set endPointOfScaleValue = (this.startValue + (range * ((si.getIntPart()*100 + si.getFracPart())/si.getSize())) / 100);
		return this.startValue + (range * ((si.getIntPart()*100 + si.getFracPart())/si.getSize())) / 100;
	}
}
