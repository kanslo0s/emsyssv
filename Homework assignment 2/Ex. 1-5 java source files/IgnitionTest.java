/**
 * The main class to test the ignition module. Iterate through
 * engine speeds from 2000 RPM to 6000 RPM with step 10 and
 * print the resulting ignition value. 
 */
public class IgnitionTest {

	public static void main(String[] args) {
		SensorValue rpmSensor = new SensorValue(1000, 0, 8000);
		IgnitionModule im = new IgnitionModule(rpmSensor); //needed for 2nd exercise
		for(int r=599; r<6000; r+=10) {
			rpmSensor.readSensor(r);
			System.out.println("RPM: "+rpmSensor.getValue()+", IGN: "+im.getIgnition());
		}
	}
}
