

int x , y , n;

int random();

int main(){
    x=0;
    y=0;
    n=random();
    __CPROVER_assume(n>0);
    while ( x != n) {
        x++;
        y++;
    }
    assert ( y==n );
}


