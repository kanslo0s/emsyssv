package rc;

class Test {

	//@ public invariant x >= 0;
	private /*@ spec_public */ int x = 0;
	
	//@ requires 1 <= d && d <= 31;
	public void test1(int d){
	}
	
	public void test2(Integer i){
			
	}
	
	public void test3(){
		x--; 
		x++;
	}
	
	public void test4(){
		x--; 
		test3();
		x++;
	}
			
	public void test5(){
		x--;	
	}	
		
	//@ ensures x == d;
	public void test6(int d){
		x = d;
		d++;
	}

	//@ ensures \result == (\forall int i; i > 3; true);
	public boolean test7(){
		return true;
	}
		
	//@ ensures \result == (\forall int i; 0 < i & i <= 34; i > -3 & i < 36);
	public boolean test8(){
		return true;
	}
}



public class RunTime {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Test t = new Test();
		//t.test1(32); System.out.println("after test1");
		//t.test2(null); System.out.println("after test2");
		t.test3(); System.out.println("after test3");
		//t.test4(); System.out.println("after test4");
		//t.test5(); System.out.println("after test5");
		t.test6(4); System.out.println("after test6");
		t.test7(); System.out.println("after test 7");
		t.test8(); System.out.println("after test 8");
	}

}
