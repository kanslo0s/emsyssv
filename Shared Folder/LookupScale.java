/** This class encapsulates lookup table scales. */

class LookupScale {
	
	/** Stores the scale (so called) break points.
	  * Scales are required to be strictly monotone,
	  * with raising values. */
	int[] values; 
	
	// INVARIANT(S)
	//@ invariant values != null;
	//@ invariant values.length > 1;
	
/** invariant (\forall int i; 0 <= i && i < values.length; values[i] < values[i+1]); */
/** invariant values.length > 1; */
	
//	/**
//	 * Construct the scale with predefined break points
//	 * @param values the array with break point values
//	 */
//	// CONTRACT 
//	//@ requires true;
//	//@ assignable values;
//	//@ ensures values == values;
//	LookupScale(int[] values) {
//		this.values = values;
//	}
	
	/**
	 * Construct a linear scale that has size break points equally
	 * distributed between min and max values. 
	 * @param min minimal value of the scale
	 * @param max maximal value of the scale
	 * @param size number of break points in the scale
	 */
	// CONTRACT 
	//@ requires min >= 0;
	//@ requires max > min;
	//@ requires size >= 2;
	LookupScale(int min, int max, int size) {
		/*@ non_null */ this.values = new int[size];
		
		/* ORIGINAL CODE!!! */
		int chunk = (max - min) / (size - 1);
		this.values[0] = min;

		//System.out.println(chunk);

		//@ assume chunk == (max - min) / (size - 1);
		//@ loop_invariant 1 <= i && i <= values.length;
		// loop_invariant (\forall int a; 0 <= a && a < i; values[a] == min + (((max - min) * a) / (size - 1)));
		//@ loop_invariant (\forall int a; 1 <= a && a < i; values[a] == values[a-1]+chunk);

		// loop_invariant values[i] == values[i-1]+chunk;
		for(int i=1; i<this.values.length-1; i++) {
			/* ORIGINAL CODE!!! */
		    this.values[i] = this.values[i-1] + chunk;
			//this.values[i] = min + (((max - min) * i) / (size - 1));
		}
	}

//	/**
//	 * Looks up a sensor value in the scale and returns the scale index
//	 * corresponding to the position of the sensor value in the scale. 
//	 * @param sv the sensor value that should be looked up the scale
//	 * @return the scale index (integral and fractional part)
//	 */
//	// CONTRACT
//	// assignable \nothing;
//	//@ requires sv.value >= sv.minValue && sv.value <= sv.maxValue;
//	/*@ pure */ ScaleIndex lookupValue(SensorValue sv) {
//		int v = sv.getValue();
//		// First get the integral part
//		// The most convenient way to lookup scales is from the end
//		int intPart = this.values.length - 1;
//		while(intPart >= 0) {
//			if(v >= this.values[intPart]) {
//				break;
//			}
//			intPart--;
//		}
//		// ASSERTION
//		//@ assert intPart >= 0 && intPart <= this.values.length -1;
//		int fracPart = 0;
//		// Check border cases
//		if(intPart == this.values.length - 1 || v < this.values[0]) {
//			// ASSERTION(S)
//			//@ assert fracPart == 0;
//			return new ScaleIndex(intPart, fracPart, this.values.length);
//		}
//		// Then calculate the fractional part
//		fracPart = (v - this.values[intPart]) * 100 / (this.values[intPart+1] - this.values[intPart]);
//		// ASSERTION(S)
//		//@ assert fracPart >= 0 && fracPart <= 99;
//		return new ScaleIndex(intPart, fracPart, this.values.length);
//	}
//
//	/**
//	 * Provide a human readable version of this object, makes 
//	 * the output of JMLUnitNG more readable.
//	 */
//	public String toString() {
//		String r = "Scale of size "+this.values.length+": [";
//		for(int i = 0; i<this.values.length; i++) {
//			r += ""+(i==0 ? "" : ", ")+values[i];
//		}
//		r += "]";
//		return r;
//	}
}