package examples;

public class example_modifies2 {

	private /*@ spec_public */ int x;
	private /*@ spec_public */ int y;
	
	//@ public invariant x >= 0;
	//@ public constraint \old(x) <= x;
	
	/*@ 
	      requires n > 0;	
	      modifies x, y;  
	      ensures x == \old(x) + n;
	 */
	public void simpleCalculation(int n) {
		x = x + n;
		doSomethingElse();
	}
	
	
    /*@
          modifies y, x;
          ensures y == \old(y) + 1 && x == \old(x);
     */
	public void doSomethingElse() {
		y = y + 1;
		x = x;
	}
	
}
