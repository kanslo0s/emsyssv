package examples;

public class example_assert {

	void test1 (int i, int j) {
		if (i <= 0 || j < 0) {
			i = 3;
			}
		else if (j < 5 && 0 < j) {
			//@ assert i > 0 && 0 < j && j < 5;
			i = 4;
		}
		else {
			//@ assert i > 0;
            //@ assert j < 0 || j >= 5;
			i = 5;
		}
	}
}
