package examples;

public class example_invariant {
	
	private /*@ spec_public */ int x;
	private /*@ spec_public */ int y;
	private /*@ spec_public */ int z;
	
	//@ public invariant x + y <= z;
	
	//@ public invariant z >= 0; 
	//@ public invariant x >= 0;
	//@ public invariant y >= 0;
	
     //@ requires a >= 0 && b >= 0 && c >= 0;
	//@ requires a + b < c;
    //@ ensures x == a && y == b && z == c;
	void changeState(int a, int b, int c){
		x = a;
		y = b;
		z = c;
	}
	

	void doubling(){
		x = 2 * x;
		y = 2 * y;
		changeState(x, y, x + y + z);
	}
}
