package examples;

public class Calculation {

	/*@ requires x > y;
	    ensures \result <= y;
	 */
	public static int calculation(int x, int y) {
		int res;
		if (2 * x + 16 < 3 * y + 24) {
			res = y - x;
		}
		else {
			res = y;
		};
		return res;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(calculation(2,1));

	}

}
