package examples;

public class example_constraint {
	
	private /*@ spec_public */ int x;
	
	//@ public constraint \old(x) <= x;
	
	void step1() {
		x = x - 1;
		step2();
		x = x + 2;
	}
	
	//@ ensures x == \old(x);
	void step2() {
	}

	

}
