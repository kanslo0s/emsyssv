package examples;

public class example_modifies {

	/*@ spec_public */ private int k; 
	
	/*@ requires k >= 0;
        ensures k == \old(k) + 1; 
    */
	void increaseK() {
    	doSomething();
        k = k + 1;
    }
	
	/*@ 
	  
	 */
	void doSomething() {
	}
}
