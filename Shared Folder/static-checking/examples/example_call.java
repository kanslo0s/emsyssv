package examples;

public class example_call {

	private /*@ spec_public */ int x = 1;
	
	//@ public invariant x >= 1;
	//@ public constraint \old(x) <= x;
	
	/*@ requires n > 0;
	    ensures x == \old(x) + n;
	 */
	public void simpleCalculation(int n) {
		add(n);
	}

	//@ requires n > 0; 
	//@ ensures x == \old(x) + n;
	public void add(int n) {
		x = x + n;
	}
}
