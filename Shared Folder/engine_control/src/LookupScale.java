/** This class encapsulates lookup table scales. */

class LookupScale {
	/** Stores the scale (so called) break points.
	  * Scales are required to be strictly monotone,
	  * with raising values. */

	int[] values;

	// INVARIANT(S)
	//@ invariant this.values.length >= 2;
	//@ invariant values != null;

	/**
	 * Construct the scale with predefined break points
	 * @param values the array with break point values
	 */
	// CONTRACT
	//@ requires true;
	//@ ensures this.values == values;
	LookupScale(int[] values) {
		this.values = values;
	}
	
	/**
	 * Construct a linear scale that has size break points equally
	 * distributed between min and max values.
	 * @param min minimal value of the scale
	 * @param max maximal value of the scale
	 * @param size number of break points in the scale
	 */
	// CONTRACT
	//@ requires size >= 2;
	//@ modifies values;
	//@ requires min >= 0;
	//@ requires max > min;
	/*the ensure beneath is not used in exercise 3: */
	//  ensures (\forall int i; 0 <= i && i < (size-1); this.values[i+1] > this.values[i]); 
	LookupScale(int min, int max, int size) {
		this.values = new int[size];
		int chunk = ((max - min) / (size - 1));
		this.values[0] = min;
		//@ loop_invariant 1 <= i;
		//@ loop_invariant i <= size;
		//@ loop_invariant (\forall int k; 0 < k && k < i; values[k] == values[k-1] + chunk);
		for(int i = 1; i<this.values.length; i++) {
			this.values[i] = this.values[i-1] + chunk;
		}
	}

	/**
	 * Looks up a sensor value in the scale and returns the scale index
	 * corresponding to the position of the sensor value in the scale. 
	 * @param sv the sensor value that should be looked up the scale
	 * @return the scale index (integral and fractional part)
	 */
	// CONTRACT
	//@ requires sv.value >= sv.minValue && sv.value <= sv.maxValue;
	ScaleIndex lookupValue(SensorValue sv) {
		int v = sv.getValue();
		// First get the integral part
		// The most convenient way to lookup scales is from the end
		int intPart = this.values.length - 1;
		while(intPart > 0) {		//changed >= to > to fix the out of bound error.
			if(v >= this.values[intPart]) {
				break;
			}
			intPart--;
			
		}
		// ASSERTION
		//@ assert intPart >= 0 && intPart <= (this.values.length - 1);
		int fracPart = 0;
		// Check border cases
		if(intPart == this.values.length - 1 || v < this.values[0]) {
			// ASSERTION(S)
			//@ assert (v >= this.values[intPart] || v < this.values[0]);
			return new ScaleIndex(intPart, fracPart, this.values.length);
		}
		// Then calculate the fractional part
		fracPart = (v - this.values[intPart]) * 100 / (this.values[intPart+1] - this.values[intPart]);
		// ASSERTION(S)
		//@ assert (v >= this.values[intPart] && v < this.values[intPart+1]) && (fracPart >= 0 && fracPart <= 99) && (fracPart == 0 ? v == this.values[intPart] : v >= this.values[intPart]);
		return new ScaleIndex(intPart, fracPart, this.values.length);
	}
	/**
	 * Provide a human readable version of this object, makes 
	 * the output of JMLUnitNG more readable.
	 */
	public String toString() {
		String r = "Scale of size "+this.values.length+": [";
		for(int i = 0; i<this.values.length; i++) {
			r += ""+(i==0 ? "" : ", ")+values[i];
		}
		r += "]";
		return r;
	}
}
