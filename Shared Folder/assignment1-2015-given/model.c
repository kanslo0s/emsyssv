#include <stdio.h>

#if MODEL==1

#define VLEN 1
#define TMAX 2

int trans(int T,int*src,int*dst){
  if (T < VLEN && T < TMAX && src[T] < 4){
    dst[T]=src[T]+1;
    return 1;
  } else {
    return 0;
  }
}

#endif

#if MODEL==2
// template for defining an executable model
#define VLEN 4
#define TMAX 3

int trans(int T,int*src,int*dst){
  switch(T){
    case 0:
      if (src[0]==0) {
        dst[0]=1;
        return 1;
      } else {
        return 0;
      }
    case 1:
      if (src[0]==1) {
        dst[0]=2;
        return 1;
      } else {
        return 0;
      }
    case 2:
      if (src[0]==1) {
        dst[0]=3;
        return 1;
      } else {
        return 0;
      }
  }
}
#endif

#if MODEL==4

#define VLEN 2
#define TMAX 2
int trans(int T,int*src,int*dst){
  if (T < VLEN && T < TMAX && src[T] < 4){
    dst[T]=src[T+100]+1;
    return 1;
  } else {
    return 0;
  }
}

#endif

#if MODEL==3 && defined(__CPROVER__)
// template for defining a verification only model

#define VLEN ???
#define TMAX ???

int trans(int T,int*src,int*dst){
  ???
}

#endif

#ifndef VLEN
#error "use -DMODEL=<number> flag to select model"
#else

struct next_state {
  int next;
  int src[VLEN];
};

typedef struct next_state *iterator_t;

void init(iterator_t iter,int *src){
  for(int i=0;i<VLEN;i++){
    iter->src[i]=src[i];
  }
  iter->next=0;
}

int next(iterator_t iter,int *dst){
	int res=0;
	
	#ifdef FIX_ARRAY
		for(int i=0;i<VLEN;i++){
			dst[i]=iter->src[i];
		}				
	#else
		for(int i=0;i<TMAX;i++){
			dst[i]=iter->src[i];
		}
	#endif
  
	while(!res&&iter->next<TMAX){
		res=trans(iter->next,iter->src,dst);
		iter->next++;
		//assert(iter->next < TMAX);
		if(iter->next == TMAX){ printf("bad\n");}
	}
	printf("bad %d\n",res);
	return res;
}

int find_depth(iterator_t iter,int *src){
  int table[VLEN*TMAX];
  int N=0;
  init(iter,src);
  for(int i=0;i<TMAX;i++){
    if(next(iter,&(table[N*VLEN]))){
      N++;
    }
  }
  int depth=0;
  for(int i=0; i<N;i++){
    int tmp=find_depth(iter,&(table[i*VLEN]));
    if(tmp>=depth){
      depth=tmp+1;
    }
	
  }
  return depth;
}

int main(int argc,char*argv[]){
  int state[VLEN];
  struct next_state ns;
  for(int i=0;i<VLEN;i++){
    state[i]=0;
  }
  int depth=find_depth(&ns,state);
  printf("depth is %d\n",depth);  
}

#endif

