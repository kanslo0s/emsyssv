MODULE worker(ready)
VAR
  state : {busy, done, home};
ASSIGN
  init(state) := done;
  next(state) := case 
                   state = busy : {busy, done};
                   state = done & ! ready : busy;
                   TRUE : home;
                 esac;
DEFINE
  message := state = busy & next(state) = done;
FAIRNESS ! state = busy;

MODULE manager(worker1, worker2, worker3, quotum)
VAR
  production1 : 0 .. quotum;
  production2 : 0 .. quotum;
  production3 : 0 .. quotum;
ASSIGN
  init(production1) := 0;
  init(production2) := 0;
  init(production3) := 0;
  next(production1) := worker1 & production1 < quotum? production1 + 1: production1;
  next(production2) := worker2 & production2 < quotum? production2 + 1: production2;
  next(production3) := worker3 & production3 < quotum? production3 + 1: production3;
DEFINE
  ready1 := production1 = quotum;
  ready2 := production2 = quotum;
  ready3 := production3 = quotum;

MODULE main
VAR
  worker1 : worker(manager.ready1);
  worker2 : worker(manager.ready2);
  worker3 : worker(manager.ready3);
  manager: manager(worker1.message, worker2.message, worker3.message, 3);
